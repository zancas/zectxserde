mod de;
mod error;

use crate::de::{from_bytes, Deserializer};
use crate::error::{Error, Result};

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
