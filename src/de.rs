//! From: https://serde.rs/impl-deserializer.html

use crate::error::{Error, Result};
use serde::Deserialize;
pub fn from_bytes<'a, T>(input: &'a [u8]) -> Result<T>
where
    T: Deserialize<'a>,
{
    unimplemented!()
}

pub struct Deserializer<'de> {
    input: &'de [u8],
}

impl<'de> Deserializer<'de> {
    pub fn from_bytes(input: &'de [u8]) -> Deserializer {
        Deserializer { input }
    }
}
